# Matplotlib Introduction

This is a presentation prepared for the

Meetup meeting

of the

Python Programming Group in Kuwait  https://www.meetup.com/Kuwait-Python-Programming-Group

on 28. August 2019


## The Presentation

The presentation is shown as a Jupyter Notebook. If you have Anaconda installed, you can just clone he project and open the .ipynb file in Anaconda

There is also a static HTML page of the entire notebook, which can just show the complete content => https://hansboden.gitlab.io/matplotlib-intro/

Because there may be difficulties copying the code snippets from the html page, there is an additional text file, where the program codes a available as plain text => matplotlib_intro.txt


