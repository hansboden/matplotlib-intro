



Introduction to Matplotlib

By Hans Boden, Aug.28, 2019




Matplotlib is a powerful Python module to draw all kinds of business or technical charts.

Matplotlib (MPL) was designed after a commercial product Matlab, which defined a standard for scientific plotting applications. MPL tries to mimick the look and feel of Matlab, which lead to the easy adoption by the scientific community, but also lead to a rather un-pythonic interface.

This introduction can show only a small subset of all the possibilities of MPL, but it should give a good starting point.

The presentation is created as a Anaconda Jupiter Notebook. Such a notebook allows to mix text, code and the graphical output of MPL, which makes is a perfect tool for this purpose. The explanations of the Anaconda environment goes beyond this introduction. Most of the code can be used in 'normal' Python scripts, without modification.

The plotting part of Matplotlib is always imported under the name plt. We also import numpy to easily work with vectors. The ´%matplotlib inline´ is a Jupiter statement, which enables the embedded display of MPL grafics.

import matplotlib.pyplot as plt

import matplotlib.image as img

import numpy as np

import mpl_intro as mylib

import struct

%matplotlib inline

The simplest application for a plot is, to draw lines. The plt.plot() function accepts x and y coordinates from two vectors.

This draws two rectangles, each made of 4 lines:

plt.title('Two simple rectangles')

x = [2,8,8,2,2]

y = [0,0,4,4,0]

print("These are the (blue) coordinates:", list(zip(x,y)))

plt.plot(x,y)

coords = [(0,2), (6,2), (6,5), (0,5), (0,2)]

x, y = zip(*coords)

plt.plot(x,y)

plt.show()

We can learn a few things from the above example:

    The imported plt object directly exposes some basic methods to produce a graphic output.
    MPL scales the plot to fit the drawn elements
    The scales of the x- and y-axis are derived from the plot data
    colors are selected automatically, each plot gets its own color
    the zip() built-in function is incredible useful (if you don't use it yet, go and discover it)

A very common use case is to draw data points along some time line. The y-vector contains the data points. If we leave out the x-vector, MPL will take the number of elements to scale the x-axis:

plt.title('A curve of random values')

y = np.random.randint(low=4, high=12, size=16)

plt.plot(y)

plt.show()

Let's assume, the values are related to some time scale, like hours. Then we have to specify the values for the x-axis.

plt.title('Temperatures over Time')

time = np.linspace(12, 19, 16)

plt.plot(time, y)

plt.yticks(range(0,17,2))

plt.show()

What is new:

    The time line is made with a numpy function, which gives a vector of 16 elements between 12 and 19
    The use of xticks() and yticks() allow to scale the view of the chart. The view can however only be extended.
    This code block used a value (y), which was created in a previous block. All the code blocks in a jupiter notebook share the same namespace, which may be both, a useful feature and a curse. To make sure that all code snippets are correct, use the 'restart kernel and run all cells' command.

Lets use the scatter function to produce some random art.

import random as rnd

plt.figure(figsize=(10, 7), dpi=80) # size in inches

plt.title('Scatter in Action')

colors = "red r orange green blue cyan gold pink grey k purple lime".split()

for _ in range(300):

    cor = rnd.choice(colors)

    px = rnd.random() * 15

    py = rnd.random() * 10

    size = 200 + rnd.random() * 5000

    plt.scatter(px,py, s=size, color=cor)  # single x,y coordinates



px = np.linspace(0, 15, 30)

py = np.linspace(0, 10, 30)

plt.scatter(px, py, s=200, color="white")  # x,y may contain a list of coordintes

py = np.linspace(10, 0, 30)

plt.scatter(px, py, s=200, color=(0.8, 0.8, 0.8))

​

plt.xticks([])

plt.yticks([])

plt.show()

Type Markdown and LaTeX: α2α2

See here:

    The figure object refers to the overall image. Here the size (in inches) and the resolution (dpi=dots per inch) can be specified.
    colors can be specified by name (there is a long list of colors abailable) or by RGB value. The values are floats between 0.0 and 1.0. Some color names like red and black can be abbreviated.
    scatter works with individual points or with lists of coordinates
    spcifying xticks and yticks as empty lists removes the scales altogether

plt.figure(figsize=(10, 4), dpi=80) # size in inches

plt.title('Sound Waves')

d = mylib.read_wav("sample")

plt.plot(d)

plt.show()

This is a sound wave of a spoken sentence. Its about 2 seconds. At 48000 frames per second this gives about 100000 samples. The samples are 16 bit, which allows for values between -32k and +32k.

The scale on the x axis shows the frames. What if we want to scale it for milliseconds ...

plt.figure(figsize=(10, 3), dpi=80) # size in inches

plt.title('Sound Waves with ms-scale')

d = mylib.read_wav("sample")

​

ticks = range(0,len(d)+10000,9600) # create ticks in 200ms distance

labels = [f"{int(p/48)}" for p in ticks] # set labels, which show the ms value

ax = plt.gca()  # get current axes

ax.set_xticks(ticks)

ax.set_xticklabels(labels)

​

plt.plot(d)

plt.show()

It is possible to scale an axis independent of the plotted data. It seems a bit tricky, but is works:

Define the label positions to the desired scale (here: 200ms distance). Then set the labels as strings, which show the calculated values (ms). For sure MPL has a built-in functionality for this, but I could not find it.

In the previous examples, the plt object received all the instructions for the graph. Here we use the gca() function to get the current axes. Then we call methods of that axes object, that are not available on the plt object. An axes is something like the inner structure of the graph. More on this later.

plt.figure(figsize=(12, 4), dpi=80) # size in inches

plt.title('Loudness wave (ms) with derivations')

loud = mylib.read_loud("sample")

deriv1 = mylib.derivation(loud, 30)

deriv2 = mylib.derivation(deriv1, 15)

​

plt.plot((0, len(loud)), (0, 0), c='grey')

plt.plot(loud, label='Loudness')

plt.plot(deriv1, color='green', label='1st Derivation')

plt.plot(deriv2, color='red', label='2nd Derivation')

plt.legend()

plt.show()

The 'loud' element is a vector of numbers giving some average loudness of the previous shown wave file. It gives one value per millisecond, no extra scaling required here. The derivations are calculated as the difference of two adjacent points (multiplied to give some visual effect).

    This code just shows, how to generate a legend to identify the different curves,

Figures and Axes

In the above example (almost) all most all effects were produced by methods of the plt object. For some of the advanced chart techniques, we must distinguish between the figure, which represents the complete graphic, and one or more axes which represent the individual plot or sub-plots, That means a figure can have more than one plot.

In the above exapmples, we did not refer to an axes, the plt methods were directed to the only one default axes.

To create more than one plot in a figure requires some magic.

plt.title('Two plots in one figure')

coords = [(5,5),(4,6),(3,10),(5,8),(7,10), (6,6), (4,4),(3,0), (5,2), (7,0), (6,4)]

coords.extend([(y,x) for x,y in coords])

x, y = zip(*coords)

plt.subplot(121)

plt.plot(x,y, color="red")

plt.subplot(122)

plt.plot(x,y, color="blue")

plt.show()

​

What happened here:

    there are two subplots on top of each other, same plot but different color
    The title does not appear as before

There is still no explicit reference to an axes. The subplot method specifies, that there are two plot areas and directs the plot instruction to one of them. The number in the subplot argument (121) is actually evaluated as:

Axes are arranged

    in 1 row
    in 2 columns

which gives 2 axes (=subplots)

    activate number 1

The subplot argument (122) gives the same layout of axes, but then

    activates number 2

fig = plt.figure(figsize=(4, 8), dpi=80) # size in inches

fig.suptitle('Two plots in one figure', fontsize=16)

coords = [(5,5),(4,6),(3,10),(5,8),(7,10), (6,6), (4,4),(3,0), (5,2), (7,0), (6,4)]

coords.extend([(y,x) for x,y in coords])

x, y = zip(*coords)

plt.subplot(211)

plt.title("subplot 1")

plt.plot(x,y, color="red")

plt.subplot(212)

plt.title("subplot 2")

plt.plot(x,y, color="blue")

plt.show()

​

A lot is going on here:

    The title is now visible as a per-plot title, which can only be specified, after we selected the subplot. To have a title for the figure, we have to obtain the figure object and call the "super title" method for it.
    the layout of the suplots has been changed to two rows and one column, so the subplots are arranged vertically
    Anyone can make sense of the way the coordinates are constructed? - If you don't use list comprehensions, this should spark your curiosity.

Ready for some strange layouts? - What will this code produce ...

fig = plt.figure(figsize=(8, 8), dpi=80) # size in inches

fig.suptitle('Many plots in a fancy layout', fontsize=16)

​

coords = [(5,5),(4,6),(3,10),(5,8),(7,10), (6,6), (4,4),(3,0), (5,2), (7,0), (6,4)]

coords.extend([(y,x) for x,y in coords])

x, y = zip(*coords)

​

plt.subplot(4,4,1)

plt.plot(x,y, color="red")

​

plt.subplot(4,4,2)

plt.plot(x,y, color="red")

​

plt.subplot(4,4,4)

plt.plot(x,y, color="red")

​

plt.subplot(4,4,5)

plt.plot(x,y, color="red")

​

plt.subplot(4,4,8)

plt.plot(x,y, color="red")

​

plt.subplot(4,4,11)

plt.plot(x,y, color="red")

​

plt.subplot(2,2,3)

plt.plot(x,y, color="blue")

​

plt.subplot(2,4,3)

plt.plot(x,y, color="green")

​

plt.subplot(4,2,8)

plt.plot(x,y, color="green")

​

plt.subplots_adjust(wspace=0.0, hspace=0.0)

​

plt.show()

​

Does this make sense?

    The rule about how subplots are aligned is remains the same. The surprising fact is, that each subplot may specify its own layout for rows and columns, and does the counting relative to this layout.
    The subplot_adjust method allows to eliminate all gaps between the subplots.
    The sequence of the painting of the subplots matters: a new subplot actually overwrites what is below its area (look at the x and y scales).
    The example had to use 3 individual numbers istead of the combined number, because (4411) would be invalid, it has to be specified as (4,4,11)


To make the layout rules easier to recognize, the same layout again, but with titles for each subplot:

fig = plt.figure(figsize=(8, 8), dpi=80) # size in inches

fig.suptitle('Many plots in a fancy layout', fontsize=16)

​

​

def subplot(l1, l2, ndx, cor):

    plt.subplot(l1,l2,ndx)

    plt.title(f"subplot {l1}{l2}{ndx}")

    plt.plot(x,y, color=cor)

    plt.xticks([])

    plt.yticks([])



coords = [(5,5),(4,6),(3,10),(5,8),(7,10), (6,6), (4,4),(3,0), (5,2), (7,0), (6,4)]

coords.extend([(y,x) for x,y in coords])

x, y = zip(*coords)

​

subplot(4,4,1, 'red')

subplot(4,4,2, 'red')

subplot(4,4,4, 'red')

subplot(4,4,5, 'red')

subplot(4,4,8, 'red')

subplot(4,4,11,'red')

​

subplot(2,2,3, "blue")

subplot(2,4,3, "green")

subplot(4,2,8, "green")

​

plt.subplots_adjust(wspace=0.3, hspace=0.3)

​

plt.show()

​

The titles should make it easier to understand the row column layout for the subplots.

The nested function made the code much shorter and nicer. Anybody uses nested function? They work different in Python than in most other languages (shared namespace).

Some methods do not work on the global plt object. That is when we use the plt.cga() method to get access to the axes element. When working with multiple subplots, it makes sense to start with the axes objects from the beginning. Often also the figure object is required.

fig, axes = plt.subplots(3)  # this returns a tuple of 3 axes objects

fig.set_size_inches(4,4)

fig.suptitle("Working with axes objects", fontsize=16)

print(len(axes))

ax0 = axes[0]

ax1 = axes[1]

ax2 = axes[2]

​

ax0.text(0.3, 0.5,"ax 0", fontsize=20)

ax1.text(0.3, 0.5,"ax 1", fontsize=20)

ax2.text(0.3, 0.5,"ax 2", fontsize=20)

plt.show()

The same is true for columns

fig, axes = plt.subplots(1,4)  # this returns a tuple of 4 axes objects

fig.set_size_inches(8,3)

fig.suptitle("Working with axes objects (columns)", fontsize=16)

print(len(axes))

ax0 = axes[0]

ax1 = axes[1]

ax2 = axes[2]

ax3 = axes[3]

​

ax0.text(0.3, 0.5,"ax 0", fontsize=20)

ax1.text(0.3, 0.5,"ax 1", fontsize=20)

ax2.text(0.3, 0.5,"ax 2", fontsize=20)

ax3.text(0.3, 0.5,"ax 3", fontsize=20)

plt.show()

fig, axes = plt.subplots(2,3)  # this returns a nested tuple of 6 axes objects

fig.set_size_inches(7,4)

fig.suptitle("Working with 2-dimensional axes layout", fontsize=16)

​

for rx, row in enumerate(axes):

    for cx, ax in enumerate(row):



        ax.text(0.2, 0.5, f"ax {rx},{cx}", fontsize=20)

        ax.set_xticks([])

        ax.set_yticks([])

​

plt.show()

fig, axes = plt.subplots(2,3)  # this returns a nested tuple of 6 axes objects

fig.set_size_inches(7,4)

fig.suptitle("Working with 2-dimensional axes layout", fontsize=16)

​

enum = enumerate  # yes, we can

# this is a generator expression

axgen = ((rx,cx,ax) for rx, row in enum(axes) for cx, ax in enum(row))

​

for rx, cx, ax in axgen:

    ax.text(0.2, 0.5, f"ax {rx},{cx}", fontsize=20)

    ax.set_xticks([])

    ax.set_yticks([])



plt.show()

fig, axes = plt.subplots(2,3)  # this returns a nested tuple of 6 axes objects

fig.set_size_inches(12,8)

fig.set_dpi(120)

fig.suptitle("Colorful Mix of plots", fontsize=16)

plt.subplots_adjust(wspace=0.4, hspace=0.4)

​

def axgen2():

    for rx, row in enumerate(axes):

        for cx, ax in enumerate(row):

            ax.set_title(f"subplot {rx},{cx}")

            # ax.set_xticks([])

            # ax.set_yticks([])

            yield rx, cx, ax



def bar_graph(rx,cx,ax):

    ax.set_title("Bar Graph - Students scores")

    students = [1, 2, 3, 4, 5, 6]

    grades = [5, 7, 3, 8, 4, 7]

    ax.bar(students, grades)

    ax.set_xticks([])

    # ax.set_yticks([])



def horiz_bar(rx, cx, ax):

    students = [1, 2, 3, 4, 5]

    grades = [5, 7, 3, 8, 4]

    colors = "blue red green orange cyan".split()

    barlist = ax.barh(students, grades)

    for ndx, cor in enumerate(colors):

        barlist[ndx].set_color(cor)

    ax.set_title("Horizontal Bar graph")

    ax.set_xlabel("Math Score")

    ax.set_ylabel('Students')

    # ax.set_xticks([])

    ax.set_yticks([])

​

def line_graph(rx,cx,ax):

    year = [2014,2015,2016,2017,2018]

    sales = [2000, 3000, 4500, 3500, 6000]

​

    ax.plot(year, sales)

    ax.set_title("Simple Line Plot")

    ax.set_xlabel('Year')

    ax.set_ylabel('Sales')

​

def pie_chart(rx, cx, ax):

    companies = 'Google Facebook Apple Microsoft IBM'.split()

    share = 20, 12, 11, 4, 3

    explode = (0.1, 0.01, 0.01, 0.01, 0.01) # explode #1 a little away



    ax.pie(share, explode=explode, labels=companies, autopct='%1.1f%%',

        shadow=False, startangle=90)

    ax.axis('equal') # Equal aspect ratio ensures that pie is drawn as a circle.

    ax.set_title('Market Share')

​

def stack_plot(rx, cx, ax):

    weeks   = [4,5,6,7,8,9]

    reading = [6,5,6,4,3,9]

    running = [3,7,4,5,2,3]

    cricket = [3,4,6,3,8,4]

    tennis  = [3,4,7,5,6,5]

    ax.plot([],[],color='r', label='reading', linewidth=5)

    ax.plot([],[],color='g', label='running', linewidth=5)

    ax.plot([],[],color='k', label='cricket', linewidth=5)

    ax.plot([],[],color='gold', label='tennis', linewidth=5)

    ax.stackplot(weeks,reading,running,cricket,tennis, colors=['r','g','k','gold'])

    ax.set_xlabel('x')

    ax.set_ylabel('y')

    ax.set_yticks(range(0,40,5))

    ax.set_title('Time spent with hobbys')

    ax.legend()

​

def show_image(rx, cx, ax):

    im = img.imread('matplotlib.png')

    ax.imshow(im)

​

myaxes = axgen2()

for chart_fn in (

    bar_graph,

    horiz_bar,

    line_graph,

    pie_chart,

    stack_plot,

    show_image,

    ):

    ax = next(myaxes)

    chart_fn(*ax)



​

fig.savefig('plot.png')

plt.show()

print("Graphic saved as plot.png")

This was the furious final.

Keep this last code snippet to reimport the mpl_intro.py module.

from importlib import reload

import mpl_intro as mylib

mylib = reload(mylib)

