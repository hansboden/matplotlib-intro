# python3

import wave
import struct
import numpy as np

def test():
    print("running tests")
    d = read_wav("sample.wav")

def read_wav(filename=''):
    print("read_wav")
    wav_file = wave.open(filename+'.wav', 'r')
    nframes = wav_file.getnframes()
    data = wav_file.readframes(nframes)
    wav_file.close()
    data = struct.unpack('{n}h'.format(n=nframes), data)
    data = np.array(data)  # We then convert the data to a numpy array.
    return data

def read_loud(filename=''):
    with open(filename+'.loud', mode='rb') as fi:
         data = fi.read()
    length_of_float = 4
    loud = struct.unpack('{n}f'.format(n=len(data)//length_of_float), data)
    loud = np.array(loud)  # We then convert the data to a numpy array.
    return loud

def derivation(wave, fact=1):
    # calculate the (math) discrete derivation of a vector
    new_wave = [0]
    for p1, p2 in zip(wave, wave[1:]):
        new_wave.append((fact * (p2 - p1)))
    return new_wave

if __name__ == '__main__':
    test()
else:
    pass #print("imported (again?)")
